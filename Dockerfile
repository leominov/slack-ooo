FROM golang:1.16-alpine3.13 AS builder
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/slack-ooo
COPY . .
RUN go build .

FROM alpine:3.13
COPY --from=builder /go/src/gitlab.qleanlabs.ru/platform/infra/slack-ooo/slack-ooo /
RUN apk add --no-cache tzdata
ENTRYPOINT [ "/slack-ooo" ]

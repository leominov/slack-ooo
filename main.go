package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"github.com/slack-go/slack/socketmode"
)

var (
	configFile = flag.String("config", "", "Path to configuration file")
	debug      = flag.Bool("debug", false, "Enable debug mode")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})

	config, err := LoadConfig(*configFile)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Infof("Configuration: %s", config.String())

	slackCli := slack.New(
		config.BotToken,
		slack.OptionAppLevelToken(config.AppToken),
		slack.OptionDebug(*debug),
		slack.OptionLog(log.New(&slackLogAdapter{
			entry: logrus.StandardLogger().WithField("source", "slack-go/slack:api"),
		}, "", 0)),
	)

	socketCli := socketmode.New(
		slackCli,
		socketmode.OptionDebug(*debug),
		socketmode.OptionLog(log.New(&slackLogAdapter{
			entry: logrus.StandardLogger().WithField("source", "slack-go/slack:socket"),
		}, "", 0)),
	)

	auth, err := slackCli.AuthTest()
	if err != nil {
		logrus.Fatal(err)
	}
	botID := auth.UserID

	logrus.Infof("Bot: %s", auth.User)

	go func() {
		err := socketCli.Run()
		if err != nil {
			logrus.Fatal(err)
		}
	}()

	go func() {
		for envelope := range socketCli.Events {
			switch envelope.Type {
			case socketmode.EventTypeConnected:
				logrus.Info("Connected")
			case socketmode.EventTypeEventsAPI:
				socketCli.Ack(*envelope.Request)
				eventPayload, ok := envelope.Data.(slackevents.EventsAPIEvent)
				if !ok {
					continue
				}
				switch eventPayload.Type {
				case slackevents.CallbackEvent:
					switch event := eventPayload.InnerEvent.Data.(type) {
					case *slackevents.MessageEvent:
						if event.User == botID {
							continue
						}
						nowParsed, _ := time.Parse(timeLayout, Now().Format(timeLayout))
						inWork := InWorkingHours(startTime, endTime, nowParsed)
						if !config.StringContainsSubstrings(event.Text) || inWork {
							continue
						}
						logrus.WithField("user", event.User).
							Infof("Current time in %s %v is in working working hours: %v",
								location.String(), nowParsed, inWork)
						_, _, err := slackCli.PostMessage(event.Channel,
							slack.MsgOptionText(config.Message, false),
							slack.MsgOptionTS(event.TimeStamp),
						)
						if err != nil {
							logrus.Error(err)
						}
					}
				}
			}
		}
	}()

	shutdownSignalChan := make(chan os.Signal, 1)
	signal.Notify(shutdownSignalChan, syscall.SIGINT, syscall.SIGTERM)

	<-shutdownSignalChan
	logrus.Info("Received shutdown signal")
}

package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNow(t *testing.T) {
	if assert.NotNil(t, Now().Location()) {
		assert.Equal(t, "Europe/Moscow", Now().Location().String())
	}
}

func TestInWorkingHours(t *testing.T) {
	assert.False(t, InWorkingHours(time.Now().Add(-5*time.Minute), time.Now().Add(5*time.Minute), time.Now().Add(10*time.Minute)))
	assert.True(t, InWorkingHours(time.Now().Add(-5*time.Minute), time.Now().Add(5*time.Minute), time.Now()))
	assert.True(t, InWorkingHours(time.Now().Add(5*time.Minute), time.Now().Add(-5*time.Minute), time.Now()))
	assert.False(t, InWorkingHours(time.Now().Add(-5*time.Minute), time.Now().Add(5*time.Minute), time.Now().Add(-10*time.Minute)))
}

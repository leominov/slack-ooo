package main

import "time"

const (
	timeLayout = "15:04"
)

var (
	location, _  = time.LoadLocation("Europe/Moscow")
	startTime, _ = time.Parse(timeLayout, "9:00")
	endTime, _   = time.Parse(timeLayout, "18:30")
)

func Now() time.Time {
	return time.Now().In(location)
}

func InWorkingHours(start, end, check time.Time) bool {
	start, end = start.In(location), end.In(location)
	if start.After(end) {
		start, end = end, start
	}
	return !check.Before(start) && !check.After(end)
}

package main

import (
	"log"
	"testing"

	"github.com/sirupsen/logrus"
)

func TestSlackLogAdapter_Write(t *testing.T) {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	l := log.New(&slackLogAdapter{
		entry: logrus.StandardLogger().WithField("source", "slack"),
	}, "", 0)
	l.Println("Hello")
}

package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	os.Unsetenv("BOT_TOKEN")
	_, err := LoadConfig("fixtures/config/not-found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "TOKEN missing")
	}

	os.Setenv("BOT_TOKEN", "ABCD")
	os.Setenv("APP_TOKEN", "FOOBAR")
	config, err := LoadConfig("fixtures/config/not-found.yaml")
	if assert.NoError(t, err) {
		assert.Equal(t, defaultConfig.ChannelID, config.ChannelID)
	}

	config, err = LoadConfig("fixtures/config/valid.yaml")
	if assert.NoError(t, err) {
		assert.Equal(t, &Config{
			BotToken:   "ABCD",
			AppToken:   "FOOBAR",
			ChannelID:  "ABCDEFG",
			Substrings: []string{"a", "b", "c"},
			Message:    defaultConfig.Message,
		}, config)
	}

	config, err = LoadConfig("fixtures/config/invalid.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "yaml")
	}
}

func TestConfig_StringContainsSubstrings(t *testing.T) {
	c := &Config{
		Substrings: []string{"foobar"},
	}
	assert.True(t, c.StringContainsSubstrings("foobar"))
	assert.False(t, c.StringContainsSubstrings("barfoo"))
}

func TestConfig_String(t *testing.T) {
	c := &Config{
		BotToken:   "AAA",
		AppToken:   "BBB",
		ChannelID:  "111",
		Substrings: []string{"222"},
		Message:    "333",
	}
	s := c.String()
	assert.NotContains(t, s, "AAA")
	assert.NotContains(t, s, "BBB")
	assert.Contains(t, s, "111")
	assert.Contains(t, s, "222")
	assert.Contains(t, s, "333")
}

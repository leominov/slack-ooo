package main

import (
	"encoding/json"
	"os"
	"strings"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

var (
	defaultConfig = &Config{
		ChannelID: "#devops",
		Substrings: []string{
			"@infra_team",
			"@infra_duty",
		},
		Message: ":zzz: Дежурный infra-команды доступен с 9:00 до 18:30 (Московское время), ваше сообщение прочитают в это время",
	}
)

type Config struct {
	BotToken   string   `json:"-" yaml:"-" envconfig:"bot_token" required:"true"`
	AppToken   string   `json:"-" yaml:"-" envconfig:"app_token" required:"true"`
	ChannelID  string   `json:"channelID" yaml:"channelID" ignored:"true"`
	Substrings []string `json:"substrings" yaml:"substrings" ignored:"true"`
	Message    string   `json:"message" yaml:"message" ignored:"true"`
}

func LoadConfig(filename string) (*Config, error) {
	c := defaultConfig
	if b, err := os.ReadFile(filename); err == nil {
		err = yaml.Unmarshal(b, c)
		if err != nil {
			return nil, err
		}
	}
	err := envconfig.Process("", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Config) StringContainsSubstrings(str string) bool {
	for _, substr := range c.Substrings {
		if strings.Contains(str, substr) {
			return true
		}
	}
	return false
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}
